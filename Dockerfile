#init a base image (Alpine)
FROM public.ecr.aws/b2m7q8s3/base-image-receipes:latest
WORKDIR /app
COPY requirements.txt .
RUN \
    apk --update add bash nano &&\
    apk add --no-cache --virtual .pynacl_deps build-base python3-dev libffi-dev &&\
    apk add --no-cache postgresql-libs &&\
    apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev &&\ 
    pip install -r requirements.txt
COPY . .
RUN chmod 777 start.sh
EXPOSE 5000